# How to run?
`python beethoven.py`

# Dependencies
 - Python >= 3.7, preferably 3.9
 - pydub for playing audio.
 - Currently, the 9CD version of all Beethoven sonatas by Igor Levit including filenames in a specific format... Or if you're in Bach-mode, all performances of Toccatas 910-916 by Glenn Gould using a specific filename format, downloadable with trusty youtube-dl:

    `youtube-dl --verbose -x --extract-audio --yes-playlist --audio-format mp3 --prefer-ffmpeg "https://www.youtube.com/watch?v=KsKOAaVHp_o&list=PLF_0Do1YJBPZFFf5SjAhd3QZ3uLHDQ2xQ"`

# Features
   - [x] Random 20 seconds can be set instead of first 20 seconds
   - [x] Select subset (first movements only, named sonatas only, everything). More to follow!
   - [x] Print score when done playing or when quit in the middle   
   - [ ] User set play mode when calling script:
     - [ ] Named mode only vs Hard mode (all sonatas, all movements)
     - [x] Random 20 seconds mode
     - [ ] Choose what to guess (exclude opus, sonata number, etc)
   - [ ] A recap function for memorization (This is Sonata X! You guessed Y!)
   - [ ] A logger that outputs to a file your guesses so you can see your improvement
   - [ ] Fast mode that logs how fast you classified each sonata
   - [ ] A more flexible parser for different editions, for instance using tagged files and a big beethoven lookup table once the opus is known
   - [ ] A more flexible parser for any large collection of music by one composer (using tagged files)
   - [ ] Spotify integration (thanks Queuebee)
