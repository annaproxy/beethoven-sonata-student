import os
from difflib import SequenceMatcher
from tools import bolden, text_markup

latin_convert = {"I": 1, "II": 2, "III": 3, "IV": 4}
sequence_matcher = SequenceMatcher()
ge_to_en = {"mondschein": "moonlight", "sturm": "tempest"}
en_to_ge = {v: k for k, v in ge_to_en.items()}

"""
♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩
         RELEVANT METHODS
♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩
"""


def german_convert(s):
    info = s.split("-")
    maj_min = info[1]
    key = info[0]
    return key.lower() if maj_min == "moll" else key


def check_common_name(s1, s2):
    """Second may be English"""
    s1 = s1.lower()
    s2 = s2.lower()
    if s1 == s2:
        return True
    sequence_matcher.set_seqs(s1, s2)
    if sequence_matcher.ratio() > 0.75:
        return True
    if s2 in en_to_ge:
        return check_common_name(s1, en_to_ge[s2])
    return False


# Filenames have this structure:
# 04.Piano Sonata No.12 in As-dur, Op.26 - IV. Allegro
# 09.Piano Sonata No.14 in cis-moll, Op.27 No.2, 'Moonlight' - I. Adagio sostenuto.mp3
def parse_sonata_file(filename):
    """A hacky parser for your sonata files from this CD edition"""
    # print(filename)
    result = dict()
    result["filename"] = filename
    if "'" in filename and "Menuetto" not in filename and "Eroe.mp3" not in filename:
        without_quotes = filename.split("'")
        result["common_name"] = without_quotes[1].strip("'")
        filename = without_quotes[0] + without_quotes[-1]
    # nummer, key, opus, deel, tempo
    filename = filename.split()
    result["number"] = int(filename[2].strip("No."))
    result["key"] = german_convert(filename[4].strip(","))
    result["opus"] = int(filename[5].strip("Op.").strip(",").strip("a"))
    movement = filename[7] if filename[7] != "-" else filename[8]
    result["movement"] = latin_convert[movement.strip(".")]
    result["tempo"] = " ".join(filename[8 if filename[7] != "-" else 9 :]).strip(".mp3")

    if "common_name" not in result:
        no = result["number"]
        j = None
        if no == 4:
            j = "Grand Sonata"
        if no == 5:
            j = "Little Pathetique"
        if no == 12:
            j = "Funeral March"
        if no == 18:
            j = "The Hunt"
        if no == 24:
            j = "A Therese"
        if no == 25:
            j = "Cuckoo"

        if j is not None:
            result["common_name"] = j
    return result


def ask_beethoven(quit_wrapper, sonata_info, correct_dict):
    """Asks some facts about the sonata.
    Args:
        sonata_info: A dictionary of the current sonata
        correct_dict: Global dictionary to store the guesses in
    """
    result = {}
    result["common_name"] = quit_wrapper(
        bolden("Is there a common name? (ENTER to say NO/skip)\t"), correct_dict
    )
    result["number"] = quit_wrapper(
        bolden("Which sonata nr is it? (ENTER to skip)\t"), correct_dict
    )
    result["movement"] = quit_wrapper(
        bolden("Which MOVEMENT in the sonata is it? (ENTER to skip)\t"), correct_dict
    )
    result["key"] = quit_wrapper(
        bolden("Which KEY is the sonata in? (ENTER to skip)\t"), correct_dict
    )
    result["opus"] = quit_wrapper(
        bolden("Which opus is it? (ENTER to skip)\t"), correct_dict
    )
    return result


def check_beethoven(result, not_done, correct_dict, sonata_info):
    one_correct = False

    if "common_name" in sonata_info:
        check = check_common_name(result["common_name"], sonata_info["common_name"])
        if check:
            correct_dict["common_name"] += 1
            one_correct = True

    mvt_correct = False
    for key in ["number" "movement", "opus"]:
        try:
            if int(result[key]) == sonata_info[key]:
                correct_dict[key] += 1
                if key != "movement":
                    one_correct = True
                else:
                    mvt_correct = True
        except:
            pass

    print(
        "{} You {} listening to Beethoven Opus {} in {}: Sonata number {} mvt {} {}".format(
            ("✅" + text_markup.GREEN)
            if one_correct
            else ("❌" + text_markup.RED)
            if not mvt_correct
            else ("☑" + text_markup.YELLOW),
            "are" if not_done else "were",
            sonata_info["opus"],
            sonata_info["key"],
            sonata_info["number"],
            sonata_info["movement"],
            text_markup.END,
        ),
        ""
        if "common_name" not in sonata_info
        else "(Also known as {})".format(sonata_info["common_name"]),
    )


def get_beethoven(sonatas_path, subset):
    # Parse everything
    sonata_list_all = []
    sonata_list_named_only = []
    sonata_list_first = []
    sonata_list_minor = []

    for cd_path in os.listdir(sonatas_path):
        cd = os.path.join(sonatas_path, cd_path)
        for file_path in os.listdir(cd):
            if "Sonata" in file_path:
                sonata_info = parse_sonata_file(file_path)
                sonata_info["filename"] = os.path.join(sonatas_path, cd_path, file_path)
                sonata_list_all.append(sonata_info)
                if "common_name" in sonata_info:
                    sonata_list_named_only.append(sonata_info)
                if sonata_info["movement"] == 1:
                    sonata_list_first.append(sonata_info)
                if sonata_info["key"].islower():
                    sonata_list_minor.append(sonata_info)

    sonata_dict = {
        "named": sonata_list_named_only,
        "all": sonata_list_all,
        "first_movements": sonata_list_first,
        "minor": sonata_list_minor,
        "named_first": [z for z in sonata_list_named_only if z in sonata_list_first],
    }
    the_sonata_list = sonata_dict[subset]
    return the_sonata_list
