import os
from tools import bolden, text_markup

# Filenames have this structure:
# Toccata in G Major, BWV 916 (Remastered)
def parse_toccata_file(filename):
    """A Youtube upload toccata parser"""
    result = dict()
    result["filename"] = filename
    filename = filename.split("Toccata in ")[1].split("(Remastered)")[0]
    key, bwv = filename.split(", BWV ")
    result["key"] = key
    result["bwv"] = bwv
    return result


def ask_bach(quit_wrapper, correct_dict, info):
    """Asks some facts about the toccata
    Args:
        info: A dictionary of the current Toccata
        correct_dict: Global dictionary to store the guesses in
    """
    result = {}
    result["bwv"] = quit_wrapper(
        bolden("Which BWV nr is it? (ENTER to skip)\t"), correct_dict
    )
    result["key"] = quit_wrapper(
        bolden("Which KEY is this Toccata in? (ENTER to skip)\t"), correct_dict
    )
    return result


def check_bach(result, not_done, correct_dict, info):
    one_correct = False
    for key in ["bwv", "key"]:
        if result[key].strip().lower() == info[key].strip().lower():
            correct_dict[key] += 1
            one_correct = True

    print(
        "{} You {} listening to J.S. Bach BWV {} Toccata in {} {}".format(
            ("✅" + text_markup.GREEN) if one_correct else ("❌" + text_markup.RED),
            "are" if not_done else "were",
            info["bwv"],
            info["key"],
            text_markup.END,
        )
    )


def get_bach(toccatas_path):
    the_toccata_list = []
    for file_path in os.listdir(toccatas_path):
        if ".mp3" in file_path:
            info = parse_toccata_file(file_path)
            info["filename"] = os.path.join(toccatas_path, file_path)
            the_toccata_list.append(info)
    return the_toccata_list
