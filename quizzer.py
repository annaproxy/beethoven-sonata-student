import os
import sys
import random
import threading
import time
import argparse  # TODO: Parse args into globals.

import subprocess
from tempfile import NamedTemporaryFile
from pydub.utils import get_player_name, make_chunks
from pydub import AudioSegment

from beethoven import ask_beethoven, get_beethoven, check_beethoven
from toccata import ask_bach, get_bach, check_bach
from tools import bolden, text_markup

"""
♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩
               UTILS
♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩
"""
_done = False
_total = None

# Logging cannot be turned off
# https://github.com/jiaaro/pydub/issues/247
# BUT I HACKED IT !
def play_song(sonata_info, process):
    process.communicate()
    # print(" (Time has run out! Please answer!)", end=" ")


def get_song_piece(song, modus, time_to_listen):
    # Clip last 2 seconds
    second_amt = int((len(song) / 1000) - 2)

    if modus == "first":
        result = song[: time_to_listen * 1000]
    elif modus == "random":
        start = random.randint(0, second_amt - time_to_listen) * 1000
        end = start + (time_to_listen * 1000)
        result = song[start:end]
    return result


def print_ending(correct_dict):
    global _done
    global _total
    print("♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩")
    print("♩♩♩♩♩♩ Thanks for playing ♩♩♩♩♩♩")
    print("♩♩  Your score ♩♩")
    for z in correct_dict:
        print(f"♩♩\t{z}:", correct_dict[z], "out of", _total["score"])
    print("♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩")

    _done = True
    sys.exit(0)


def quit_wrapper(s, correct_dict):
    global _done
    a = input(s)
    if a == "quit":
        print_ending(correct_dict)
        _done = True
        return None
    return a


def ask(ask_func, check_func, process, time_to_listen, correct_dict, *args):
    """Asks some facts about the sonata.
    Args:
        ask_function: A function that asks questions and stores them accordingly.
        process: A subprocess in which ffmpeg is playing the file.
        args: args for the ask_func  and check_func
    """
    start_time = time.time()
    result = ask_func(quit_wrapper, correct_dict, *args)
    end_time = time.time()

    spend_answering = end_time - start_time
    not_done = spend_answering < time_to_listen

    check_func(result, not_done, correct_dict, *args)

    if not_done:
        want_to_skip = quit_wrapper(
            "\t (Skip this one? Press ENTER to skip, c+ENTER to continue)", correct_dict
        ).lower()
        if not want_to_skip == "c":
            process.terminate()
    print()


"""
♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩
               MAIN 
♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩
"""


def quiz(args):
    global _total
    if args.music == "beethoven-sonata":
        ask_func = ask_beethoven
        check_func = check_beethoven
        the_list = get_beethoven(args.path, "all")
        welcome = "♩ Welcome to Beethoven Tester! ♩"

    elif args.music == "bach-toccata":
        ask_func = ask_bach
        check_func = check_bach
        the_list = get_bach(args.path)
        welcome = "♩♩ Welcome to Toccata Tester! ♩♩"
    random.shuffle(the_list)
    total_amt = len(the_list)

    # Bookkeeping
    _total = {"score": 0}
    correct_dict = {z: 0 for z in the_list[0].keys()}

    # Welcome message
    print("♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩")
    print(welcome)
    print("♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩♩")
    print("At any point, you can type `quit` to quit and see your final score.")
    print()

    for nr, info in enumerate(the_list):

        song = AudioSegment.from_mp3(info["filename"])
        song = get_song_piece(song, args.modus, args.time)
        PLAYER = get_player_name()
        print(f"======= ♬ New Piece Playing! ♬ {nr+1}/{total_amt} ===========")

        with NamedTemporaryFile("w+b", suffix=".wav") as f:
            song.export(f.name, "wav")
            with open(os.devnull, "w") as fp:
                process = subprocess.Popen(
                    [PLAYER, "-nodisp", "-autoexit", "-hide_banner", f.name],
                    stdout=fp,
                    stderr=fp,
                )
            _total["score"] += 1

            thread_player = threading.Thread(
                target=play_song,
                args=(
                    info,
                    process,
                ),
            )
            thread_listener = threading.Thread(
                target=ask,
                args=(ask_func, check_func, process, args.time, correct_dict, info),
            )

            thread_player.start()
            thread_listener.start()
            thread_player.join()
            thread_listener.join()

            if _done:
                break
    if not _done:
        print_ending(correct_dict)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Classical music quizzer 9000")
    parser.add_argument(
        "--time",
        type=int,
        default=20,
        required=False,
        help="Amount of seconds you get to identify the piece.",
    )
    parser.add_argument(
        "--path",
        type=str,
        default="/home/anna/Music/Glenny",
        help="Path to the relevant folder",
    )  # Beethoven - Complete Piano Sonatas (Igor Levit)/
    parser.add_argument(
        "--music",
        required=False,
        default="bach-toccata",
        choices=["bach-toccata", "beethoven-sonata"],
    )
    parser.add_argument(
        "--modus", required=False, default="random", choices=["random", "first"]
    )
    parser.add_argument(
        "--guess",
        default=["bwv", "key"],
        nargs="+",
        help="What do you want to guess? Uhhh this doesnt work at the moment",
    )
    quiz(parser.parse_args())
